import Nick from '../img/team/Nick.png';
import Sasha from '../img/team/Sasha.png';
import Demyan from '../img/team/Demyan.png';
import Maksim from '../img/team/Maksim.png';

export const TeamMembers = [
  {
    name: "Курбатов\nДемьян",
    description: "Наш лидер и вдохновитель. Учится на ФИИТе, работает над серверной частью проектов. Может часами размышлять о великом под Beatles и Джонна Леннона",
    skills: "Skills: JavaScript, Python, TypeScript, NestJS, Django, PostgreSQL, Docker, Docker Swarm",
    photo: Demyan,
  },
  {
    name: "Граматчикова\nАлександра",
    description: "Дизайн-сторона команды, учится на Прикладной информатике, пьёт литрами кофе и ругается на плохой UX жизни (нет, ну правда!)",
    skills: "Skills: UI/UX, Brand design, аналитика и презентации",
    photo: Sasha,
  },
  {
    name: "Мозганов\nНиколай",
    description: "Программист-оркестр :) В команде занимает роль Frontend разработчика, постоянно хочет спать",
    skills: "Skills: JavaScript, Python Backend, ML, C# + Unity",
    photo: Nick,
  },
  {
    name: "Башкардинов\nМаксим",
    description: "Так же занимается FrontEnd разработкой в нашей команде, любит делать многофункциональные интерфейсы",
    skills: "Skills: React, Redux Toolkit, Saga, TypeScript",
    photo: Maksim,
  },
]


