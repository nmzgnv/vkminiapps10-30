export const Colors = {
  green: "#CCEF28",
  white: '#FFFFFF',
  gray: '#818C99',
  black: '#0F1114',
}
