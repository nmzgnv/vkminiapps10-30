import React, {useEffect, useState} from "react";
import './index.css';
import {Colors} from "../constants/Colors";
import {Div, Title} from "@vkontakte/vkui";
import useWindowDimensions from "../utils/useWindowDimensions";

const maxMobileHeight = 710;

const TeamMemberDescription = ({teamMember, isMobile=false, isRight=false}) => {
  const textAlignment = {textAlign: isRight? 'right': 'left'}
  return (
    <Div className={'text-container'}>
      <Title className={'name-text'} level="1" weight="bold" style={{color: Colors.green, whiteSpace: "pre-wrap", ...textAlignment}}>{teamMember.name}</Title>
      <p style={{color: Colors.white, ...textAlignment}}>{teamMember.description}</p>
      <p style={{color: Colors.gray, ...textAlignment}}>{teamMember.skills}</p>
    </Div>
  )
}

const TeamMember = ({teamMember, reverse=false, needSpace=true}) => {
  const { height, width } = useWindowDimensions();
  const [isMobile, setIsMobile] = useState(width < maxMobileHeight);
  const needSpaceStyle = needSpace ? {marginBottom: 90} : null
  const alignmentStyle = width < 840 ? null : (reverse ? {marginLeft: 100 } : {marginRight: 100 })

  useEffect(() => {
    setIsMobile(width < maxMobileHeight)
  }, [width])


  return (
    <div style={alignmentStyle}>
      {reverse && !isMobile ? (
        <div className={'horizontal-container'} style={needSpaceStyle}>
          <TeamMemberDescription teamMember={teamMember} isMobile={isMobile} isRight={true}/>
          <img className={'image'} src={teamMember.photo}></img>
        </div>
      ) : (
        <div className={'horizontal-container'} style={needSpaceStyle}>
          <img className={'image'} src={teamMember.photo} style={{marginRight: 10}}></img>
          <TeamMemberDescription teamMember={teamMember} isMobile={isMobile}/>
        </div>
      )}
    </div>
  );
}

export default TeamMember;


