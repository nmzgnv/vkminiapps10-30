import React, { useState } from 'react';
import { View, AdaptivityProvider, AppRoot } from '@vkontakte/vkui';
import '@vkontakte/vkui/dist/vkui.css';
import Cutaway from "./panels/Cutaway";
import './panels/Cutaway.css';

const App = () => {
	const [activePanel, setActivePanel] = useState('cutaway');
	return (
		<AdaptivityProvider className={'panel-container'}>
			<AppRoot className={'panel-container'}>
				<View activePanel={activePanel} className={'panel-container'} >
					<Cutaway id={'cutaway'} />
				</View>
			</AppRoot>
		</AdaptivityProvider>
	);
}

export default App;
