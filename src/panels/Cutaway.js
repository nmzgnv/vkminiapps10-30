import React from 'react';
import {Panel, PanelHeader, Header, Button, SimpleCell, Group, Cell, Div, Avatar, Text, Title} from '@vkontakte/vkui';
import TeamMember from "../components/TeamMember";
import {Colors} from "../constants/Colors";
import {TeamMembers} from "../constants/TeamMembers";
import './Cutaway.css';
import Logo from '../img/Logo.png';
import useWindowDimensions from "../utils/useWindowDimensions";

const Cutaway = ({ id, go}) => {
  const { height, width } = useWindowDimensions();

  return (
    <Panel className={'panel-container'} id={id}>
      <Group className={'panel-container'}>
        <Div style={{alignItems: 'center', justifyContent: 'center', justifyItems: 'center', backgroundColor: Colors.black}}>
          <img src={Logo} style={{maxWidth: Math.min(width - 50, 500), marginTop:260, marginBottom:46, display: 'block', marginLeft: 'auto', marginRight: 'auto'}}/>
          <Title level="3" weight="medium" style={{color: Colors.white, marginBottom: 273 , textAlign:'center'}}>А ты точно в тонусе?</Title>
        </Div>
        <div style={{backgroundColor: Colors.black}}>
          {TeamMembers.map((member, key) => {
            return <TeamMember teamMember={member} reverse={key % 2 != 0} needSpace={true}/>
          })}
        </div>
      </Group>
    </Panel>
  );
}

export default Cutaway;
